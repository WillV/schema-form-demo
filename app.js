var app = angular.module('app', ['schemaForm', 'mgcrea.ngStrap']);

app.controller('FormController', function($scope, $timeout) {
    // get schema or form from server?

    $scope.schema = {
        type: "object",
        title: "Config",
        properties: {
            name: {
                title: "Config Name",
                type: "string"
            },
            trace: {
                title: "Trace Name",
                type: "string"
            },
            destination: {
                title: "Destination",
                type: "number",
                minimum: 0,
                maximum: 65536
            },
            timeWindow: {
                title: "Time Window",
                type: "number",
                minimum: 0,
                maximum: 10
            },
            comment: {
                title: "Comment",
                type: "string",
                maxLength: 200,
                validationMessage: "Exceeds character limit!"
            },
            multiselect: {
                    title: "Properties",
                    type: "array",
                    items: {type: "string"},
                    maxItems: 7,
                    description: "Please select no more than 7 properties."
            },
        },
        required: [
            "name",
            "trace",
            "destination",
            "multiselect"
        ]
    };

    $scope.form = [
        {
            key: "name",
            placeholder: "Copley"
        },
        {
            key: "trace",
            placeholder: "trace1"
        },
        {
            key: "destination",
            placeholder: 123
        },
        {
            key: "timeWindow",
            placeholder: 1
        },
        {
            key: "multiselect",
            type: "strapselect",
            placeholder: "Please select a property.",
            options: {
                multiple: "true",
            },
            validationMessage: "Please select a min of 1 and max of 7 properties!",
            titleMap: [
                {value: "value1", name: "Velocity", category: "value1"},
                {value: "value2", name: "Current", category: "value1"},
                {value: "value3", name: "Position", category: "value2"},
                {value: "value4", name: "text4", category: "value1"},
                {value: "value5", name: "text5", category: "value1"},
                {value: "value6", name: "text6", category: "value1"},
                {value: "value7", name: "text7", category: "value2"},
                {value: "value8", name: "text8", category: "value1"}
            ]
        },
        
        {
            key: "comment",
            type: "textarea",
            placeholder: "Make a comment"
        },
        {
            type: "section",
            htmlClass: "row",
            items: [
                {
                    type: "submit",
                    style: "btn-success",
                    htmlClass: "col-xs-2",
                    title: "Load"
                },
                {
                    type: "submit",
                    style: "btn-primary",
                    htmlClass: "col-xs-2",
                    title: "Save",
                    onClick: "saveTest()"
                },
                {
                    type: "button",
                    htmlClass: "col-xs-2",
                    style: "btn-default",
                    title: "Cancel",
                    onClick: "cancel()"
                }
            ]
        },
    ];

    $scope.model = {};

    $scope.showConfigModal = true;
    $scope.showConfigForm = true;
    $scope.configMessage = {
        show: false,
        text: "",
        style: {}
    };
    
    /* Multiple select from dynamically loaded list via synchronous callback function
           Callback must return an array of value/name objects (see static list above).
        {
            "key": "multiselectDynamic",
            "type": 'strapmultiselect',
            "options": {
                "multiple": "true"
                "callback": $scope.callBackMSD
                }
        },
        */
    // Config form buttons

    $scope.submitForm = function(form) {
        console.log('LOAD!!')

        $scope.$broadcast('schemaFormValidate');

        if (form.$valid) {
            console.log('Form valid! Post to "/creat_config".')
            // put this in POST success callback
            displayConfigMessage("Saved Successfully!", "green", true)    
        };
    };

    $scope.load = function() {
        console.log('LOAD BUTTON CLICKED')

        // reset form 
        $scope.model = {};
        $scope.myForm.$setPristine()
    }

    $scope.openConfig = function() {
        //$("#copley-config-modal").modal('show');
        $scope.showConfigModal = true;
    };

    $scope.cancel = function() {
        //$("#copley-config-modal").modal('hide');
        $scope.showConfigModal = false;
    }
    
    function displayConfigMessage(msg, color, closeModal) {
        $scope.showConfigForm = false;

        $scope.configMessage.show = true;
        $scope.configMessage.text = msg;
        $scope.configMessage.style = {"color": color, "text-align": "center"}

        //$scope.$apply();
        
        $timeout(function () {
            $scope.showConfigForm = true;
            $scope.configMessage.show = false;
        }, 1500);  
      
    };

    function postSuccess() {
        // put this in POST success callback
            // Success message a
        $scope.showConfigForm = false;
        $scope.showConfigMessage = true;

        $timeout(function () {
            $scope.showConfigForm = true;
            $scope.showConfigMessage = false;
        }, 1500);

        // reset form 
        $scope.model = {};
        $scope.myForm.$setPristine()
    }



});